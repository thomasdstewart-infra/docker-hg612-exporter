FROM docker.io/debian:bullseye as build

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install build-essential ca-certificates git golang && \
    #git clone https://github.com/jakekeeys/hg612-exporter.git && \
    git clone https://github.com/thomasdstewart/hg612-exporter.git && \
    cd hg612-exporter && \
    go build -ldflags "-extldflags \"-static\"" .

FROM docker.io/debian:bullseye
LABEL name="docker-hg612-exporter"
LABEL url="https://gitlab.com/thomasdstewart-infra/docker-hg612-exporter"
LABEL maintainer="thomas@stewarts.org.uk"

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y --no-install-recommends install ca-certificates

COPY --from=build /hg612-exporter/hg612-exporter /bin
ENTRYPOINT ["/bin/hg612-exporter"]
